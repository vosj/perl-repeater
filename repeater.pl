#!/usr/bin/env perl

# Copyright © 2021 by Jeff Vos <jeff@jeffvos.dev>
# ⚔️

use strict;
use warnings;
use utf8;
use v5.10;
use Getopt::Std;
use Scalar::Util::Numeric qw(isint);
use Term::ReadKey;

$| = 1;

# Get terminal dimensions
(my $wchar, my $hchar, my $wpixels, my $hpixels) = GetTerminalSize();

# Set default message, repeat limit, and window width
my $message = "Hello world!";
my $repeat_limit = 40;
my $window_width = $wchar;

# Define global variables
my $repeat_count;
my $message_length = length($message) + 2;
my $columns = int($window_width / $message_length);
my $remainder = 0;
my $error_check = 0;

# Define options hash for Getopt
my %options = ();

# Reduce $repeat_limit by 1 to allow for later math
$repeat_limit--;

# Print out help screen and exit
sub help() {
	# Increase $repeat_limit back to normal for correct display in help screen
	$repeat_limit++;
	print	"Repeats a given message a random number of times.\n\n" .
	"Options:\n" .
	" -h\t\tShow this help.\n" .
	" -m <str>\tSet the message to be repeated.\n" .
	"\t\t(default: \"$message\")\n" .
	" -c <int>\tSet column number explicitly.\n" .
	"\t\t(this overrides -w)\n" .
	" -w <int>\tSet terminal width.\n" .
	"\t\t(default: $window_width)\n" .
	" -r <int>\tSet upper repeat limit.\n" .
	"\t\t(default: $repeat_limit)\n" .
	" -R <int>\tExplicitly set number of repeats (non-random).\n" .
	" -v\t\tVerbose output.\n";
	exit 0;
}

# Print details about upcoming printout if called with '-v'
sub verbose() {
	print "Repeating \"$message\" $repeat_count ";
	if($repeat_count == 1) {
		print "time";
	} else {
		print "times";
	}
	print " in $columns columns in a $window_width character wide terminal.\n\n";
}

# Check for command line options and set variables accordingly
sub set_options() {
	getopts("m:c:w:r:R:vh", \%options);

	# Check '-m' flag for a custom message
	if(defined $options{m}) {
		$message = $options{m};
		# Reevaluate $message_length for the custom message
		$message_length = length($message) + 2;
		# Reevaluate $columns for the custom message
		$columns = int($window_width / $message_length);
	}

	# Check '-r' flag for a custom repeat limit
	if(defined $options{r}) {
		if(isint $options{r}) {
			$repeat_limit = $options{r};
			# Reduce $repeat_limit by 1 to allow for later math
			$repeat_limit--;
		} else {
			# Print warning and log error if non-integer was given for '-r'
			warn "Non-integer value specified for -r\n";
			$error_check++;
		}
	}

	# Check '-c' flag for a custom column count
	if(defined $options{c}) {
		if(isint $options{c}) {
			$columns = $options{c};
		} else {
			# Print warning and log error if non-integer was given for '-c'
			warn "Non-integer value specified for -c\n";
			$error_check++;
		}
		# If '-c' is not set, check '-w' flag for custom terminal width
	} elsif(defined $options{w}) {
		if(isint $options{w}) {
			$window_width = $options{w};
			# Reevaluate column count based on new terminal width
			$columns = int($window_width / $message_length);
		} else {
			# Print warning and log error if non-integer was given for '-w'
			warn "Non-integer value specified for -w\n";
			$error_check++;
		}
	}

	# Check '-R' flag for custom static repeat count
	if(defined $options{R}) {
		if(isint $options{R}) {
			$repeat_count = $options{R};
		} else {
			# Print warning and log error if non-integer was given for '-R'
			warn "Non-integer value specified for -R\n";
			$error_check++;
		}
	} else {
		# Calculate random repeat count between 1 and repeat limit if
		# repeats were not defined statically
		$repeat_count = int(rand($repeat_limit)) + 1;
	}
}

# Print repeated content to the terminal
sub printout() {
	my $counter = 1;
	while($counter <= $repeat_count) {
		print $message;
		if($window_width > $message_length) {
			my $position = $counter / $columns;
			$remainder = $position - int $position;
			# If instance is in the last column, print a newline
			# Otherwise, print two spaces
			if($remainder == 0) {
				print "\n";
			} else {
				print "  ";
			}
			# If the message is longer than the terminal is wide, simply
			# print newlines between each instance
		} else {
			print "\n";
		}
		$counter++;
	}
	# Print another newline if the printout didn't end on the last column
	if($remainder != 0) {
		print "\n";
	}
}

# Main sequence
sub main() {
	# Call set_options subroutine
	set_options();

	# Exit if errors were encountered
	if($error_check > 0) {
		exit $error_check;
	}

	# Call help screen if '-h' flag was given
	if(defined $options{h}) {
		help();
	}

	# Issue verbose output if '-v' flag was given
	if(defined $options{v}) {
		verbose();
	}

	# Call printout subroutine
	printout();
}

main();
