# perl-repeater

A simple Perl script that repeats a message in the terminal.

## Script Options

* `-h` - show the help screen
* `-m <str>` - set a custom message; default: "Hello world!"
* `-c <int>` - set a static column count; default: dynamic based on terminal width. This overrides the `-w` flag
* `-w <int>` - set a custom terminal width; default: determined automatically at execution
* `-r <int>` - set a custom upper repeat limit - the number of times the message is repeated will be between 1 and this number; default: 40
* `-R <int>` - set a static repeat count; this overrides the `-r` flag
* `-v` - Show verbose output about the settings before printing the messages
